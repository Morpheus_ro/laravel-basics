<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Post;

class ExampleTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel');
    }

    public function testBasicForm()
    {
        $this->visit('/')
            ->type('some query', '#search')
            ->press('Search')
            ->see('Search results from "some query"')
            ->seePageIs('/some-results');
    }

    public function testFactoryOnPosts()
    {
        $post = factory(Post::class)->create();

        $this->visit('posts')
            ->see($post->title);
    }

    public function testAdmin()
    {
        $alecs = factory('App\User')->create(['name' => 'Alecs']);

        $this->actingAs($alecs)
            ->visit('admin')
            ->see('Hello Alecs');
    }
}
