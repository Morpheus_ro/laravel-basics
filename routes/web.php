<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//DB::listen(function ($query) {
//    var_dump($query->sql, $query->bindings);
//});

//class Mailer
//{
//
//}
//
//class RegistersUsers
//{
//    protected $mailer;
//
//    public function __construct(Mailer $mailer)
//    {
//        $this->mailer = $mailer;
//    }
//
//    public function setMailer(Mailer $mailer)
//    {
//        $this->mailer = $mailer;
//    }
//}

//App::bind('RegistersUsers', function(){
//    return new RegistersUsers(new Mailer);
//});

// injection the service with the view composer
//View::composer('stats', function ($view) {
//    $view->with('stats', app('App\Stats'));
//});

use App\Events\UserHasRegistered;

Route::get('begin', function () {
    flash('You are now logged in', 'success');
    return redirect('/');
});

Route::get('stats', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');
});

Route::post('some-results', function () {
    return sprintf('Search results from "%s"', Request::input('search'));
});

Route::get('posts', function () {
    return view('posts')->with('posts', App\Post::all());
});

Route::get('about', 'PagesController@home');
Route::get('register-users', function (RegistersUsers $registersUsers) {
    var_dump($registersUsers);
});
Route::get('testing-container', 'PostsController@index');

Route::get('cards', 'CardsController@index');
Route::get('cards/{card}', 'CardsController@show');
Route::post('cards/{card}/notes', 'NotesController@store');
Route::get('notes/{note}/edit', 'NotesController@edit');
Route::patch('notes/{note}', 'NotesController@update');

Auth::routes();

Route::get('/dashboard', 'HomeController@index');

Route::get('/admin', ['middleware' => 'admin:Anca', function () {
    return 'Hello Alecs';
}]);

Route::get('/events', function () {
    return view('pusher');
});

Route::get('/broadcast', function () {
    event(new UserHasRegistered('Alecs'));

    return 'Done';
});
