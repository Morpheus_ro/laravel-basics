<!DOCTYPE html>
<head>
    <title>Pusher Test</title>
    <script src="https://js.pusher.com/3.2/pusher.min.js"></script>
    <script>
        (function() {
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('2e3c0e1522d634b857d7', {
                cluster: 'eu',
                encrypted: true
            });

            var channel = pusher.subscribe('test');

            channel.bind('my_event', function(data) {
                console.log(data);
            });

        })();
    </script>
</head>
<body>
    <h1>Events</h1>
</body>
