<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Laravel</title>

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

    @yield('header')

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>

    <div class="container">
        @yield('content')
    </div>

    @yield('footer')

</body>
</html>