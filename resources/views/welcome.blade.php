@inject('stats', 'App\Stats')

@extends('layout')

@section('content')
    @include ('flash')
    <h1>The welcome page goes here</h1>
    @include ('stats')

    <form action="some-results" method="POST">
        {{ csrf_field() }}
        <input type="text" name="search" id="search" >

        <button type="submit">Search</button>
    </form>

    </form>
@stop
