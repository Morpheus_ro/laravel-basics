<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function home()
    {
        $people = ['test1','test2', 'test3'];

        return view('pages.about', compact('people'));
    }
}
