<?php

namespace App\Http\Controllers;

use App\Card;
use App\Note;
use Illuminate\Http\Request;

use App\Http\Requests;

class NotesController extends Controller
{
    public function store(Request $request, Card $card)
    {
        $this->validate($request, [
            'body' => 'required|min:10',
            // email validation on unique email column from users table
//            'email' => 'email|unique:users,email'
        ]);
//        $note = new Note($request->all());
// This is for login user will be implemented
//        $note->user_id = Auth::id();

//        $note->user_id = 1;

        $card->addNote(
            new Note($request->all()),
            1
        );
        return back();

    }

    public function edit(Note $note)
    {
        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note)
    {
        $note->update($request->all());

        return back();
    }
}
