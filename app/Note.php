<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable =['body', 'user_id'];

    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function path()
    {
        return '/notes/' . $this->id;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
