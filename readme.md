# to initiate tinker

php artisan tinker

# To listen(dump) to queries in tinker

DB::listen(function($query){ var_dump($query->sql);});

# get card all notes

$card = App\Card::first();

$card->notes;

# get card notes method

$card = App\Card::first();

$card->notes();

# get all cards for a note

$note = App\Note::first()

$note->card

# save a note to a card

$note = new App\Note;

$note->body = 'Here is another note';

$card = App\Card::first();

$card->notes()->save($note);

$card->notes;